﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace TestApp01
{
    class Program
    {
        static void Main(string[] args)
        {
            string filename = "sample.txt";
            Directory.SetCurrentDirectory(@"C:\Users\412429\Desktop");

            //string test;

            FileStream fs = new FileStream(filename, FileMode.Open);

            int i = 0;
            while ((i = fs.ReadByte()) != -1)
            {
                Console.Write((char)i);
            }

            fs.Close();
            fs.Dispose();

#if DEBUG
            Console.WriteLine("続行するには何かキーを押してください...");
            Console.ReadKey();
#endif
        }
    }
}
